const { factorial, div_check } = require('../src/util')

//gets the expect and assert functions from chai to be used
const { expect, assert } = require('chai')

//Test Suites are made up of collection of test cases that should be executed together
//describe() - keyword is used to group tests together
describe('test fun factorials', () => {
    //"it()" accepts two parameters
    //string explaining what the test should do
    //callback function which the actual test
    it('test fun factorial 5! is 120', () => {
        const product = factorial(5);
        //expect - returning expected and actual value
        expect (product).to.equal(120);
    })

    it('test fun factorial 1! is 1', () => {
        const product = factorial(1)
        //asert - checking function is return correct results
        assert.equal(product, 1)
    })

    it('test fun factorial 0! is 1', () => {
        const product = factorial(0)
        //asert - checking function is return correct results
        assert.equal(product, 1)
    })

    it('test fun factorial 4! is 24', () => {
        const product = factorial(4);
        //expect - returning expected and actual value
        expect (product).to.equal(24);
    })

    it('test fun factorial 10! is 3628800', () => {
        const product = factorial(10);
        //expect - returning expected and actual value
        expect (product).to.equal(3628800);
    })

})



// 1. In your test.js, create test cases in the test fun factorials test suite to check if the factorial function's result for 0!, 4!, and 10! using assert and/or expect.
// 2. In your util.js, create a function called div_check in util.js that checks a number if it is divisible by 5 or 7.
// a. If the number received is divisible by 5, return true.
// b. If the number received is divisible by 7, return true.
// c. Return false if otherwise
// 3. Create 4 test cases in a new test suite in test.js that would check if the functionality of div_check is correct.
// 4. Initialize your local git repository, add the remote link and push to git with the commit message of “Add activity code S2".
// 5. Add the link in Boodle.

describe('divisible by 5 or 7', () => {

    it('test 100 divisible by 5', () => {
        const div = div_check(100);
        //expect - returning expected and actual value
        expect (div).to.be.true;
    })

    it('test 49 divisible by 7', () => {
        const div = div_check(49);
        //expect - returning expected and actual value
        expect (div).to.be.true;
    })

    it('test 30 divisible by 5', () => {
        const div = div_check(30);
        //expect - returning expected and actual value
        expect (div).to.be.true;
    })

    it('test 56 divisible by 7', () => {
        const div = div_check(56);
        //expect - returning expected and actual value
        expect (div).to.be.true;
    })

})