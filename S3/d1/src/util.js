function factorial(num){
    if (num === 0) return 1;
    if (num === 1) return 1;
    return num * factorial(num - 1);
}

function div_check(num){
    if ((num % 5 == 0 || num % 7 === 0)) {
        return true;
    } else {
        return false;
    }
}

const names = {
    "Brandon": {
        "name": "test",
        "age": "12"
    },
    "Brandon": {
        "name": "server",
        "age": "13"
    }
}

module.exports = {
    factorial: factorial,
    div_check: div_check,
    names: names
}